Este projeto visa investigar algoritmos e técnicas em hardware para a execução eficiente de operações de salto em máquinas virtuais.
Máquinas virtuais são programas de computador que emulam uma interface para execução de outros programas, compilados para a interface sendo emulada.
Esta tecnologia está presente em diversos sistemas computacionais e é utilizada desde o suporte à linguagens de programação de alto nível, como na máquina virtual Java, até a implementação de processadores com projeto integrado de hardware e software.
A maioria das máquinas virtuais existentes na literatura utiliza técnicas de emulação similares, como interpretação e tradução dinâmica de binários. De fato, as duas técnicas são muitas vezes utilizadas de forma colaborativa, em uma mesma máquina virtual, para acelerar o processo de emulação e tornar a máquina virtual eficiente. Dada a importância do processo de emulação nas máquinas virtuais, o objetivo deste projeto de pesquisa é a investigação de
algoritmos e técnicas em hardware para acelerar o processo de emulação em máquinas virtuais. Este projeto será focado na investigação de técnicas para acelerar a emulação de saltos diretos e indiretos com ênfase em benchmarks com cargas de trabalho representativas de aplicações de produtividade ou de grande porte como sistemas operacionais, ferramentas de escritório e de acesso à internet.



