\subsection{Técnicas em \software\ para emulação de salto indireto}
\label{subsection:tecnicas_software}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inline caching
\paragraph{\Inlinecaching\ (predição de saltos indiretos por \software):}
Durante a tradução de um fragmento terminado em um salto indireto, o mecanismo
\inlinecaching\ substitui a instrução de salto por uma sequência de testes que
compara o endereço emulado obtido durante a execução com endereços emulados de
fragmentos já presentes na \cache\ de traduções. Caso um par de endereços
emulados idênticos seja encontrado, o controle da execução é transferido para o
endereço traduzido equivalente. Somente caso todas as comparações falhem, o
controle é transferido para o gerenciador da emulação. A
\reffigure~\ref{algorithm:inline_caching} ilustra a técnica. A quantidade de
comparações que se faz é arbitrária e depende do nível de complexidade dos
mecanismos de decisão, como veremos na \refsection~\ref{section:revisao}. Essa
técnica pode ser vista como uma implementação do mecanismo \inlinecaching\
desenvolvido para a linguagem orientada a objetos Smalltalk-80.
Deutsch~\cite{Deutsch1984POPL} descreve como essa técnica é utilizada para se
encontrar o endereço correto da implementação de um método de uma classe em
tempo de execução, visto que em linguagens orientadas a objetos nem sempre é
possível determinar em tempo de compilação a qual classe pertence um objeto e,
consequentemente, a qual implementação de um método, uma chamada se refere.
Hiser e outros~\cite{Hiser2011TACO} introduziram uma especialização nessa
técnica baseada em perfilamento da execução que melhora a sua eficiência.
Durante a execução, saltos indiretos são monitorados e a quantidade de vezes que
um determinado endereço alvo é tomado é anotada. Quando a quantidade de
execuções de um salto indireto atinge um certo limiar, os alvos mais
frequentemente tomados são adicionados à sequência de comparações do
\inlinecaching. Essa técnica permite manter o número de comparações
relativamente pequeno e melhor adaptado ao perfil de execução de cada salto.

\input{alg/inline_caching.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Speculative chaining
\paragraph{\Speculativechaining~\cite{Witchel1996SIGMETRICS}:} Com esta técnica,
toda instrução de salto indireto encontrada no final de um fragmento de tradução
é convertida em um salto direto e incondicional para um endereço traduzido alvo,
equivalente a um endereço emulado especulativo, determinado no momento da
tradução dinâmica ou através de informação de \profiling. Por se tratar de um
salto especulativo, um trecho adicional de código é inserido no início do
fragmento alvo, que verifica, em tempo de execução, se o controle chegou ao
endereço correto. A verificação é feita através de uma comparação entre o
endereço emulado obtido durante a execução e o endereço especulativo determinado
durante a tradução, como pode ser observado na
\reffigure~\ref{figure:speculative_chaining}. Uma variação dessa técnica é
apresentada por Bruening~\cite{Bruening2003CGO} e por Bala~\cite{Bala2000PLDI}
para ferramentas que utilizam \traces\ como unidade básica de tradução. Um
\trace\ é uma sequência de instruções que engloba diversos blocos básicos e que
representa um caminho frequentemente executado do grafo de fluxo de controle. A
formação de um \trace\ se estende através de um salto indireto, englobando um
único fragmento alvo do salto, determinado de maneira especulativa. No entanto,
como execuções subsequentes do \trace\ podem tomar caminhos diferentes, um
trecho de código é adicionalmente inserido para verificar se o controle da
execução deve continuar no \trace\ (\fallthrough) ou retornar ao gerenciador da
emulação.

\input{pgf/speculative_chaining.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Daisy
\paragraph{Expansão de tradução:} Essa técnica foi desenvolvida para a
ferramenta Daisy~\cite{Ebcioglu1997ISCA} e utiliza um método de particionamento
do espaço virtual de endereçamento da máquina hóspede em que é possível
determinar a relação entre endereço emulado e traduzido através de uma fórmula
relativamente simples: $n \times N + VLIW\_BASE$, onde $n$ é o endereço emulado,
$N$ é o fator de expansão da memória e $VLIW\_BASE$ é um \offset\ que representa
o início da área de traduções. Com isso, o problema da resolução do alvo de
saltos indiretos é trivialmente resolvido. No entanto, o Daisy tem como unidade
básica de tradução uma página (tipicamente quatro \kbytes) e no momento da
tradução, somente a entrada da página, os alvos de arestas de laços e chamadas
de funções são considerados entradas válidas, isto é, pontos para onde é
possível saltar e que tem seu endereço calculável pela fórmula acima. Alvos de
saltos indiretos podem ser, portanto, pontos de entrada inválidos. Para melhorar
a eficiência da emulação, o código da página alvo do salto indireto é
retraduzido no momento de sua execução, o código da página é reorganizado e o
endereço alvo é marcado como ponto de entrada válido para execuções
subsequentes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IBTC
\paragraph{\IBTC~\cite{Scott2001VIRGINIA}:} A técnica \IBTC\ consiste em um
mecanismo que transforma um salto indireto em uma consulta a uma tabela simples
de mapeamentos entre endereços emulado e traduzido. Esta tabela funciona como
uma \cache\ e armazena apenas um subconjunto da tabela de mapeamentos mantida
pelo gerenciador da emulação. A consulta à tabela é indexada pelo resultado de
uma operação de espalhamento sobre o endereço emulado e é implementável em
poucas instruções de forma que o código de consulta pode ser adicionado à
própria tradução do fragmento. O conteúdo obtido da tabela é um par de endereços
emulado e traduzido equivalente. O endereço obtido pela execução é comparado com
o endereço emulado obtido da tabela e, caso sejam idênticos, o controle da
execução é transferido para o endereço traduzido correspondente. Caso contrário,
o controle é devolvido ao gerenciador da emulação. O gerenciador ainda pode
encontrar uma equivalência entre endereços emulado e traduzido, uma vez que
mantém uma tabela mais completa de mapeamentos. Caso não haja equivalência, o
gerenciador dispara a tradução de um novo fragmento de código e atualiza tanto a
tabela de mapeamento global quanto a \IBTC. Outras ferramentas, como o
Dynamo~\cite{Bala2000PLDI}, também postergam a transferência de controle para o
gerenciador da emulação, inserindo código traduzido que faz uma busca na tabela
de mapeamentos, no entanto, essas ferramentas não contam com uma \cache\
simplificada da tabela, por isso acessam a tabela do gerenciador. Outra
característica que distingue a técnica \IBTC\ do mecanismo utilizado pelo Dynamo
é o posicionamento do código de busca. O Dynamo~\cite{Bala2000PLDI} posiciona o
código em uma rotina e substitui os saltos indiretos por chamadas a essa rotina,
enquanto o Strata~\cite{Scott2001VIRGINIA} substitui os saltos indiretos por
pequenos trechos de código responsáveis por fazer a busca. Os desempenhos das
duas abordagens foram medidos por Hiser~\cite{Hiser2011TACO} e não apresentaram
diferenças significativas.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sieve
\paragraph{\Sieve~\cite{Sridhar2006VEE, Luk2005PLDI}:} A técnica \Sieve\ pode
ser vista como um mecanismo que utiliza instruções, em vez de memória de dados,
para armazenar os mapeamentos entre endereços emulados e traduzidos. Instruções
de salto indireto são transformadas, durante a tradução, em saltos para cadeias
de \sievebuckets, que são pequenos trechos de código com a seguinte função:
comparar um endereço emulado obtido durante a execução com um endereço emulado
obtido durante a tradução e armazenado na forma de uma constante no próprio
código do \sievebucket. Caso os endereços emulados sejam iguais, o controle da
execução é transferido para o endereço traduzido equivalente, caso contrário,
para o próximo \sievebucket\ na cadeia. Esse processo se repete até que aconteça
um acerto na comparação de endereços emulados, ou até que se atinja o último
\sievebucket, que retorna o controle da execução para o gerenciador da emulação.
Diversas cadeias de \sievebuckets\ são mantidas e elas são endereçadas pelo
resultado de uma operação de espalhamento entre o endereço emulado obtido
durante a execução e uma máscara pré-definida. A \reffigure~\ref{figure:sieve}
ilustra a técnica.

\input{pgf/sieve.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fast returns
\paragraph{\Fastreturns~\cite{Scott2001VIRGINIA}:} Esta é uma técnica específica
para tratar o caso especial dos saltos indiretos no retorno das funções.
Tipicamente, instruções de retorno transferem o controle da execução para um
endereço previamente salvo pela instrução que invocou a função correspondente,
em geral, o endereço da instrução subsequente a esta. Esta técnica propõe
alterar, durante a tradução, a instrução de chamada, de forma que esta salve na
pilha do programa, não mais o endereço emulado, referente ao código original,
mas o endereço traduzido, referente a um fragmento de código na \cache\ de
traduções. Dessa forma, também durante a tradução, instruções de retorno podem
ser mantidas inalteradas, uma vez que se tem certeza de que o alvo será um
endereço na \cache\ de traduções. A \reffigure~\ref{figure:fast_returns} ilustra
a técnica. Uma das desvantagens dessa técnica é que ela só funciona caso o
endereço de retorno não seja alterado pela função em questão, isto é, entre a
chamada e o retorno. Embora esse seja um comportamento padrão, há alguns casos
especias onde alterações no endereço de retorno ocorrem, como em algumas rotinas
da \glibc\ e exceções em C++. Outra desvantagem é que alguns dados armazenados
na pilha do programa durante a emulação diferem dos dados armazenados na
execução nativa. Esta diferença torna a emulação detectável por parte do
programa sendo emulado e pode fazer com que a emulação do programa apresente um
comportamento distindo da execução nativa.

\input{alg/fast_returns.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Shadow stack
\paragraph{\Shadowstack~\cite{Chernoff1998MICRO}:} Esta técnica foi desenvolvida
para tratar especificamente o salto indireto no retorno de funções. Durante a
tradução, instruções de chamada de função são substituídas por trechos de código
que empilham, em uma pilha alternativa, a \shadowstack, os endereços emulado e
traduzido de retorno da função. Adicionalmente, instruções de retorno são
modificadas, durante a tradução, para que desempilhem a última entrada adiciona
à \shadowstack, verifiquem se o endereço emulado obtido durante a execução é
igual ao endereço emulado obtido da pilha e, em caso positivo, saltem para o
endereço traduzido correspondente. Caso os endereços emulados comparados sejam
diferentes, um outra técnica para emulação de saltos é utilizada, ou o controle
é devolvido ao gerenciador da emulação. Hazelwood e
Klauser~\cite{Hazelwood2006CASES} descrevem a mesma técnica, mas nomeiam-na
\software\ \RAS. Hiser~\cite{Hiser2011TACO} também utiliza um nome distinto para
a mesma técnica: \RATS\ (pilha de tradução de endereços de retorno).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Return cache
\paragraph{\Returncache~\cite{Sridhar2006VEE}:} Diferentemente da técnica
\shadowstack, que mantém uma pilha de endereços equivalentes emulado e
traduzido, a técnica \returncache\ mantém uma tabela de espalhamento apenas com
endereços traduzidos de retorno indexada pelo resultado de uma operação de
espalhamento sobre o endereço da função na aplicação original, isto é, um
endereço emulado único para cada função. Instruções de chamada de função são
traduzidas de forma a atualizar o conteúdo da \returncache\ com o seu endereço
traduzido de retorno antes de saltar para o código traduzido da função. Por
outro lado, instruções de retorno são modificadas de forma a retornar o controle
da execução para o endereço contido na \returncache, sem verificar a corretude
deste. Assim sendo, a tradução de múltiplas chamadas~- em locais distintos do
código~- para uma mesma função sempre atualizam a mesma entrada da \returncache\
com seus múltiplos endereços de retorno, enquanto a tradução da instrução de
retorno sempre acessa a mesma entrada da \returncache, uma vez que só existe uma
tradução para a função. Tendo em vista que o endereço de retorno da função pode
ser alterado entre a chamada e o retorno, como em algumas funções da \glibc, um
código de verificação é adicionado na sequência da instrução de chamada. Este
código verifica se o endereço emulado de retorno obtido durante a execução é
equivalente ao endereço emulado de retorno que seria obtido caso nenhuma
alteração ocorresse durante a execução da função e retorna o controle da
execução para o gerenciador da emulação caso a verificação falhe.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Empty line needed to define a new paragraph

