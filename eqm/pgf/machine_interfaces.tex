\begin{figure}
	\rmfamily
	\centering
	\subfloat[ABI]{
		\label{subfigure:machine_interfaces_abi}
		\begin{tikzpicture}[node distance=0ex, outer sep=0ex, inner sep=0ex]
			\scriptsize
			% Background fill
			\node [draw=black!20,fill=black!20,rectangle,minimum width=36ex,minimum height=8ex] (bottomfill) {};
			\path (bottomfill.north west) node [anchor=south west,draw=black!20,fill=black!20,rectangle,minimum width=24ex,minimum height=10ex] (upperfill) {};
			\path (bottomfill.south west) node [anchor=south west,rectangle,minimum width=24ex,minimum height=18ex] (nofill) {Máquina};
			% Outer frame
			\path (bottomfill.south west) node [anchor=south west,draw=black,rectangle,minimum width=36ex,minimum height=28ex] (outerframe) {};
			\coordinate (O) at ($(outerframe.north west)!0.5!(outerframe.north east)$);
			\path [draw] (O) node [below=1ex] {Aplicativos};
			% ABI line
			\path (upperfill.north west)+(-2ex,+0ex) node (leftthorn) {};
			\path (bottomfill.north east)+(+2ex,+0ex) node [right] (rightthorn) {ABI};
			\path [draw,thick] (leftthorn) --
			                   (upperfill.north west) --
			                   (upperfill.north east) --
			                   (upperfill.south east) --
			                   (bottomfill.north east) --
			                   (rightthorn);
			% System calls
			\path (upperfill.north west) node [anchor=west,rectangle,minimum width=24ex,minimum height=4ex] (sysarrows) {};
			\path [draw] (sysarrows.north west) node [above right=2ex,anchor=west] (syscalltext) {chamadas de sistema};
			\path [draw,->,>=latex] ($(sysarrows.north west)!0.2!(sysarrows.north east)$) -- ($(sysarrows.south west)!0.2!(sysarrows.south east)$);
			\path [draw,->,>=latex] ($(sysarrows.north west)!0.4!(sysarrows.north east)$) -- ($(sysarrows.south west)!0.4!(sysarrows.south east)$);
			\path [draw,->,>=latex] ($(sysarrows.north west)!0.6!(sysarrows.north east)$) -- ($(sysarrows.south west)!0.6!(sysarrows.south east)$);
			\path [draw,->,>=latex] ($(sysarrows.north west)!0.8!(sysarrows.north east)$) -- ($(sysarrows.south west)!0.8!(sysarrows.south east)$);
		\end{tikzpicture}
	}
	\subfloat[ISA]{
		\label{subfigure:machine_interfaces_isa}
		\begin{tikzpicture}[node distance=0ex, outer sep=0ex, inner sep=0ex]
			\scriptsize
			% Background fill
			\node [draw=black!20,fill=black!20,rectangle,minimum width=36ex,minimum height=8ex] (bottomfill) {Máquina};
			% Fly line
			\path (bottomfill.north west)+(+0ex,+1ex) node [] (flyorigin) {};
			\path (bottomfill.north east)+(+0ex,+1ex) node [] (flyend) {};
			\path [draw] (flyorigin) node [above right=2ex,anchor=west] (fotext) {Sistema};
			\path [draw] (flyend) node [above left=2ex,anchor=east] (fetext) {Usuário};
			\path [draw] (flyorigin) -- ($(flyorigin)!0.4!(flyend)$);
			\path [draw] ($(flyorigin)!0.5!(flyend)$) -- (flyend);
			% Inner frame
			\path (bottomfill.north west) node [anchor=south west,rectangle,minimum width=24ex,minimum height=10ex] (innerframe) {};
			\coordinate (C) at ($(innerframe.north west)!0.5!(innerframe.north east)$);
			\path [draw] (C) node [below=1ex] {SO};
			\coordinate (A) at (flyorigin -| innerframe.north east);
			\path [draw] (innerframe.north west) --
			             (innerframe.north east) --
			             (A);
			% Outer frame
			\path (bottomfill.south west) node [anchor=south west,draw=black,rectangle,minimum width=36ex,minimum height=28ex] (outerframe) {};
			\coordinate (O) at ($(outerframe.north west)!0.5!(outerframe.north east)$);
			\path [draw] (O) node [below=1ex] {Aplicativos};
			% ISA line
			\path (bottomfill.north west)+(-2ex,+0ex) node (leftthorn) {};
			\path (bottomfill.north east)+(+2ex,+0ex) node [right] (rightthorn) {ISA};
			\path [draw,thick] (leftthorn) --
			                   (bottomfill.north west) --
			                   (bottomfill.north east) --
			                   (rightthorn);
		\end{tikzpicture}
	}
	\ifx\avoidcaption\undefined
		\caption{Interfaces de máquinas}
		\label{figure:machine_interfaces}
	\fi
\end{figure}
