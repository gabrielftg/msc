# The project name. 
PROJECT_NAME=principal

# The list of bib files
BIB_FILES=bibliografia.bib

# The default message for make
help:
	@echo -e "Usage: make [option]"
	@echo -e " option:"
	@echo -e "  - clean    : remove \"$(FILES_TO_REMOVE)\" files" 
	@echo -e "  - all      : generates $(PROJECT_NAME).{dvi,pdf,ps}" 
	@echo -e "  - dvi      : generates $(PROJECT_NAME).dvi"
	@echo -e "  - ps       : generates $(PROJECT_NAME).ps"
	@echo -e "  - pdf      : generates $(PROJECT_NAME).pdf"

# The Latex program
LATEX=latex

# The Bibtex program
BIBTEX=bibtex

# Remove file program
RM=rm

# DVI to PDF program
DVI_TO_PDF=dvipdf

# PS to PDF program
PS_TO_PDF=ps2pdf14

# DVI to PS program
DVI_TO_PS=dvips

# The dvi dependencies
DVI_DEPENDENCIES=$(PROJECT_NAME).aux $(PROJECT_NAME).bbl

# Files to be removed on clean rule.
FILES_TO_REMOVE=*.log *.lof *.loa *.lot *.aux *.toc *.bbl *.blg *.out *.dvi

all: pdf ps

div: $(PROJECT_NAME).div

ps: $(PROJECT_NAME).ps

pdf: $(PROJECT_NAME).pdf

$(PROJECT_NAME).pdf: $(PROJECT_NAME).ps
	@echo "********************************************************************************"
	@echo "** Generating the $${PROJECT_NAME}.pdf file."
	@echo "********************************************************************************"
	$(PS_TO_PDF) $(PROJECT_NAME).ps $(PROJECT_NAME).pdf

$(PROJECT_NAME).ps: $(PROJECT_NAME).dvi
	@echo "********************************************************************************"
	@echo "** Generating the $${PROJECT_NAME}.ps file."
	@echo "********************************************************************************"
	$(DVI_TO_PS) -t letter $(PROJECT_NAME).dvi -o $(PROJECT_NAME).ps

$(PROJECT_NAME).dvi: $(DVI_DEPENDENCIES)
	@echo "********************************************************************************"
	@echo "** Generating the $${PROJECT_NAME}.dvi file."
	@echo "********************************************************************************"
	$(LATEX) $(PROJECT_NAME).tex

$(PROJECT_NAME).bbl: $(PROJECT_NAME).aux $(BIB_FILES)
	@echo "********************************************************************************"
	@echo "** Gerenating the $${PROJECT_NAME}.bbl file."
	@echo "********************************************************************************"
	$(BIBTEX) $(PROJECT_NAME)
	@echo "********************************************************************************"
	@echo "** Updating the .aux file with bbl info."
	@echo "********************************************************************************"
	$(LATEX) $(PROJECT_NAME).tex

$(PROJECT_NAME).aux: $(PROJECT_NAME).tex
	@echo "********************************************************************************"
	@echo "** Updating the .aux file."
	@echo "********************************************************************************"
	$(LATEX) $(PROJECT_NAME).tex

clean:
	@echo "********************************************************************************"
	@echo "** Cleaning files."
	@echo "********************************************************************************"
	$(RM) -f $(FILES_TO_REMOVE)

.PHONY: all clean dvi ps pdf help
